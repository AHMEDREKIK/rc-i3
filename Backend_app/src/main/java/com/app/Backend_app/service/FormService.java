package com.app.Backend_app.service;

import com.app.Backend_app.model.*;
import com.app.Backend_app.repository.FormRepository;
import com.app.Backend_app.repository.ReferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormService {
    @Autowired
    private FormRepository formRepository;
    @Autowired
    private ReferenceRepository referenceRepository;
    public List<Form> fetchAllFormsFromBackend(){
        return formRepository.findAll();
    }
    public void updateModified(Form form) {
        formRepository.deleteById(form.getId());
        formRepository.save(form);
    }
    public void saveReference(String referenceMarque, List<Ref> listeOfRef) {
        for(int j=0;j<listeOfRef.size();j++){
            String referenceString =listeOfRef.get(j).getReference();
            Reference reference = new Reference(referenceMarque, referenceString);
            referenceRepository.save(reference);
        }
    }
    public List<Reference> fetchAllReferencesFromBackend() {
        return referenceRepository.findAll();
    }
}
