package com.app.Backend_app.model;
import java.util.List;

public class ReferenceList {
    private String marque;

    private List<Ref> references;


    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public List<Ref> getReferences() {
        return references;
    }

    public void setReferences(List<Ref> references) {
        this.references = references;
    }




}
