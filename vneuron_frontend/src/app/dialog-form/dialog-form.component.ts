import { ModifierFormComponent } from './../modifier-form/modifier-form.component';
import { Form } from './../Models/Form';
import { Component, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-dialog-form',
  templateUrl: './dialog-form.component.html',
  styleUrls: ['./dialog-form.component.css']
})

export class DialogFormComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public form: Form, private _dialog:MatDialog) {
  }

  closeDialog() {
    this._dialog.closeAll();
  }

  openModifyDialog(){
    this._dialog.open(ModifierFormComponent,{data: this.form})
  }
}
