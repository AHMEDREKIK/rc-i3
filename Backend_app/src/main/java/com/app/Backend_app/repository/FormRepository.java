package com.app.Backend_app.repository;

import com.app.Backend_app.model.Form;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormRepository extends JpaRepository<Form,Integer> {

}
