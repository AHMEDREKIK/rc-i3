import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';
import {HttpClient} from'@angular/common/http';
import {shareReplay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private _http:HttpClient) { }
  public loginUserFromRemote(user:User):Observable<any>{
    return  this._http.post<any>("http://localhost:8090/login",user)
  }
  public regintrationUser(user:User):Observable<any>{
    return  this._http.post<any>("http://localhost:8090/registration",user)
  }
}
