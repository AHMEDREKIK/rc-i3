package com.app.Backend_app;

import com.app.Backend_app.model.Form;
import com.app.Backend_app.model.Reference;
import com.app.Backend_app.repository.FormRepository;
import com.app.Backend_app.repository.ReferenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class BackendAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendAppApplication.class, args);
	}

	private static final Logger log = LoggerFactory.getLogger(BackendAppApplication.class);

	@Bean
	public CommandLineRunner demo(FormRepository formRepository, ReferenceRepository referenceRepository) {
		return (args) -> {
			formRepository.save(new Form(1, "Hyundai", 2018, 220, 5, "rouge",50000,"https://www.pngmart.com/files/21/Accent-Transparent-PNG.png"));
			formRepository.save(new Form(2, "Renault", 2020, 240, 5, "gris",60000,"https://location.carrefour.fr/drupal-static/2022-02/location-renault-grand-scenic-aile-droit_0.png"));
			formRepository.save(new Form(3, "Audi", 2020, 300, 8, "jaune",120000,"https://www.pngplay.com/wp-content/uploads/7/Yellow-Car-Background-PNG-Image.png"));
			formRepository.save(new Form(4, "Suzuki", 2021, 260, 6, "doré",55000,"https://www.pngplay.com/wp-content/uploads/7/Car-Download-Free-PNG.png"));
			formRepository.save(new Form(5, "Mercedes", 2021, 300, 8, "blanc",150000,"https://www.pngplay.com/wp-content/uploads/7/Car-PNG-HD-Quality.png"));
			referenceRepository.save(new Reference("Mercedes","C 180"));
			referenceRepository.save(new Reference("Mercedes","C 200"));
			referenceRepository.save(new Reference("Renault","Clio 4"));
			referenceRepository.save(new Reference("Renault","Clio 5"));
		};
	}

}