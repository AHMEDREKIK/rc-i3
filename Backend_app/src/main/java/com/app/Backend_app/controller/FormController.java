package com.app.Backend_app.controller;

import com.app.Backend_app.model.Form;
import com.app.Backend_app.repository.FormRepository;
import com.app.Backend_app.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FormController {

    @Autowired
    private FormService formService;
    @Autowired
    private FormRepository formRepository;


    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/allforms")
    public List<Form> fetchAllForm() {
        return formService.fetchAllFormsFromBackend();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/modifiedform")
    public Form receiveModified(@RequestBody Form form) {
        int formId = form.getId();
        if (formId != 0) {
            formService.updateModified(form);
        }
        return form;
    }
}
