package com.app.Backend_app.model;
import javax.persistence.*;

@Entity
@Table(name = "TableReference")
public class Reference {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int referenceId;

    @Column(name = "_marqueForm")
    private String marque;
    @Column(name = "_reference")
    private String reference;

    public Reference() {
    }

    public Reference(String marque, String  reference) {

        this.marque = marque;
        this.reference = reference;
    }

    public int getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(int referenceId) {
        this.referenceId = referenceId;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
