package com.app.Backend_app.controller;

import com.app.Backend_app.model.Reference;
import com.app.Backend_app.model.Ref;
import com.app.Backend_app.model.ReferenceList;
import com.app.Backend_app.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReferenceController {
    @Autowired
    private FormService formService;


    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/modifiedreference")
    public String receiveModified(@RequestBody ReferenceList listeOfReference) {
        String  referenceMarque = listeOfReference.getMarque();
        List<Ref> listeOfRef = listeOfReference.getReferences();
        if (referenceMarque != null) {
            formService.saveReference(referenceMarque, listeOfRef);
        }
        return "It worked";
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/allreferences")
    public List<Reference> fetchAllReferences() {
        return formService.fetchAllReferencesFromBackend();
    }
}
