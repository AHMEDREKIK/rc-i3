package com.app.Backend_app.controller;

import com.app.Backend_app.model.User;
import com.app.Backend_app.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegistrationController {
    @Autowired
    private RegistrationService service;
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/registration")
    public User registerUser(@RequestBody User user) throws  Exception{
        String tempEmailId =user.getEmailId();
        if(tempEmailId != null && !"".equals(tempEmailId)){
            User userobj = service.fetchUserByEmailId(tempEmailId);
            if(userobj != null){
                throw new Exception("user with" +tempEmailId+"is already exist");
            }
        }
        User userObj= null;
        userObj = service.saveUser(user);
        service.saveUser(user);
        return userObj;
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/login")
    public User loginUser(@RequestBody User user) throws  Exception{
        String tempEmailId = user.getEmailId();
        String tempPass = user.getPassword();
        User userObj =null ;
        if(tempEmailId != null && tempPass != null){
            userObj = service.fetchUserByEmailIdAndPassword(tempEmailId,tempPass);
        }
        if(userObj == null){
            throw  new Exception("bad credentials");
        }
        return userObj ;
    }
}
