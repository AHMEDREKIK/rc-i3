import { AuthguardGuard } from './../shared/authguard.guard';
import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(private auth: AuthService){}

  onClickLogout(){
    this.auth.IsLoggedIn = false;
  }
}
