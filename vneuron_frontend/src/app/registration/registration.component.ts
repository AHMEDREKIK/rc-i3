import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';
import { NgForm } from '@angular/forms';
import { interval, Subscription, switchMap, timeInterval, timer } from 'rxjs';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent{


  passwordConfirmationFailed:boolean = false;
  passwordConfirmationTxt :string = '';

  user =new User();
  msg ='';



  constructor(private _service:RegistrationService ,private _router:Router, private _toastr: ToastrService){}

  registrationUser(){
    this._service.regintrationUser(this.user).subscribe(
      data =>{console.log("response recieved");
      this._router.navigate(['']);
      this._toastr.success("Congrations!", "You create an account successfully.");
        },error=>{ console.log("exception occured");
        this._toastr.error("This email is used ");
    this.msg ="This email is used ";})
  }

  confirmPassword() {
    if (this.user.password === this.passwordConfirmationTxt && this.passwordConfirmationTxt !='') {
      this.passwordConfirmationFailed = true;
    } else {
      this.passwordConfirmationFailed = false;
    }
  }
}
