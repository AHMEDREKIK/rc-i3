package com.app.Backend_app.repository;

import com.app.Backend_app.model.Reference;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReferenceRepository extends JpaRepository<Reference,Integer> {
}
